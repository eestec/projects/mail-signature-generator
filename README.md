# EESTEC Mail Signature Generator

This is a web tool written in plain HTML, CSS, and vanilla Javascript that generates signatures for the international level of EESTEC, as well as branches.

Can be found at [sig-gen.eestec.net](https://sig-gen.eestec.net).
